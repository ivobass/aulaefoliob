#pragma once
#include "infojogo.h"
#include <string>


class InfoJogoFutebol : public InfoJogo {
public:
  InfoJogoFutebol(std::string &namePlayer, std::string &posPlayer, int scorePlayer, int timePlayer);

  void adicionar(InfoJogoFutebol valor);
  void validarFut();
  void imprimeJogo();
  void modalidade();
  void sair();

private:
  std::string m_namePlayer;
  std::string m_posPlayer;
  int m_scorePlayer;
  int m_timePlayer;

  //variaveis
    std::string m_nameTeamA;
    std::string m_nameTeamB;
    int m_scoreTeamA;
    int m_scoreTeamB;
    int m_total;
    std::string m_players;
    std::string m_changes;
    int m_playerScore;
    int m_playerTime;
};


