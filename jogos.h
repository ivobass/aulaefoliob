#pragma once
#include "infojogo.h"
#include <string>
#include <vector>

class Jogos : public InfoJogo{
public:
  Jogos (std::string &namePlayer, std::string &posPlayer, int scorePlayer, int timePlayer);

  void adicionarJogos(std::unique_ptr<Jogos> valor);
  void ordena();
  void imprimeJogo();
  void SmartPointer();

private:
  std::vector<std::unique_ptr<Jogos>> m_jogosFutebol;
  std::vector<std::unique_ptr<Jogos>> m_jogosBasquetebol;
//variaveis
  std::string m_namePlayer;
  std::string m_posPlayer;
  int m_scorePlayer;
  int m_timePlayer;

  std::string m_nameTeamA;
  std::string m_nameTeamB;
  int m_scoreTeamA;
  int m_scoreTeamB;
  int m_total;
  std::string m_players;
  std::string m_changes;
  int m_playerScore;
  int m_playerTime;
};

