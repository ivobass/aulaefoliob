#include "infojogo.h"
#include <iostream>
#include <nlohmann/json.hpp>
#include <fstream>

InfoJogo::InfoJogo(std::string nameTeamA, std::string nameTeamB, int scoreTeamA, int scoreTeamB)
     : m_nameTeamA(nameTeamA), m_nameTeamB(nameTeamB), m_scoreTeamA(scoreTeamA), m_scoreTeamB(scoreTeamB)
{
  // construtor vazio
}


// Getters
std::string InfoJogo::getTeamA() const {
    return m_nameTeamA;
    }
std::string InfoJogo::getTeamB() const {
    return m_nameTeamB;
}
int InfoJogo::getScoreTeamA() const {
    return m_scoreTeamA;
}
int InfoJogo::getScoreTeamB() const {
    return m_scoreTeamB;
}
//resultado final de cada partida
int InfoJogo::getScoreTotal () const{
    return m_total;
}
//jugadores e posições
std::string InfoJogo::getPlayers() const{
    return m_players;
}
//substituções e expulsões
std::string InfoJogo::getChanges() const{
    return m_changes;
}
//score individual por jugador
int InfoJogo::getPlayerScore() const{
    return m_playerScore;
}
//tempo em que jugador marca
int InfoJogo::getPlayerTime() const{
    return m_playerTime;
}


//Setters
bool InfoJogo::setScoreTeamA(int valor){
    if(valor < 0 || valor >100){
        return false;
    }
    m_scoreTeamA = valor;
    return true;

}
bool InfoJogo::setScoreTeamB(int valor){
    if(valor < 0 || valor >100){
        return false;
    }
    m_scoreTeamB = valor;
    return true;

}

// função imprimir na tela
void InfoJogo::imprime() const {
    nlohmann::json j;
    //ficheiro json
    std::ofstream file("jogos.json");
    file << std::setw(4) << j << m_nameTeamA << " " << m_nameTeamB << " " << m_scoreTeamA
           << " " << m_scoreTeamB << std::endl;
    // fin criação json
    // ficheiro txt
    std::ofstream myfile;
    myfile.open ("alunos.txt");
    if(myfile.is_open()){
    std::cout << m_nameTeamA << " " << m_nameTeamB << " " << m_scoreTeamA
              << " " << m_scoreTeamB << std::endl;
    myfile << m_nameTeamA << " " << m_nameTeamB << " " << m_scoreTeamA
              << " " << m_scoreTeamB << std::endl;
    }
}



