#include "infojogofutebol.h"
#include "infojogo.h"
#include "jogos.h"
#include <iostream>
#include <algorithm>\


// no construtor do b, você inicializa os membros de a:

//ClasseFilha::ClasseFilha(int valor) : ClassePai(valor)
//{
//}

//Jogos::Jogos (std::string &namePlayer, std::string &posPlayer, int scorePlayer, int timePlayer) : InfoJogo (std::string nameTeamA, std::string nameTeamB, int scoreTeamA,int scoreTeamB)
//    : m_namePlayer(namePlayer), m_posPlayer(posPlayer), m_scorePlayer(scorePlayer), m_timePlayer(timePlayer),m_nameTeamA(nameTeamA),m_nameTeamB(nameTeamB),m_scoreTeamA(scoreTeamA),m_scoreTeamB(scoreTeamB)
//{
// // construtor vazio
//}

//Jogos::Jogos() {
//  // apagar registros
//  // procurar jogo
//}

//valida jogadores titulares futebol
void Jogos::adicionarJogos(std::unique_ptr<Jogos> valor) {
  m_jogosFutebol.push_back(std::move(valor));   // validar ou introduzir jogadores move indica ao compilador que ele não quer uma cópia, mas sim, mover o elemento.
}

void Jogos::ordena(){
    std::sort(std::begin(m_jogosFutebol),std::end(m_jogosFutebol));

}
//Imprime os jugadores na tela
void Jogos::imprimeJogo() {
  for (auto& valoratual : m_jogosFutebol) {
    valoratual->imprimeJogo();
  }
  return;
}
